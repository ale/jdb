package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/Knetic/govaluate"
)

func findList(l []interface{}, keys []string) (interface{}, error) {
	if len(keys) < 1 {
		return nil, errors.New("empty path")
	}

	key, rest := keys[0], keys[1:]

	idx, err := strconv.Atoi(key)
	if err != nil {
		return nil, fmt.Errorf("bad index '%s': %v", key, err)
	}

	if idx < 0 || idx > len(l) {
		return nil, fmt.Errorf("index out of bounds: %d != [0, %d)", idx, len(l))
	}

	value := l[idx]
	if len(rest) > 0 {
		return doFind(key, rest, value)
	}
	return value, nil
}

func findMap(n map[string]interface{}, keys []string) (interface{}, error) {
	if len(keys) < 1 {
		return nil, errors.New("empty path")
	}

	key, rest := keys[0], keys[1:]

	value, ok := n[key]
	if !ok {
		return nil, fmt.Errorf("%s not found", strings.Join(keys, "."))
	}

	// Are there more parts to the path? If so, recurse.
	if len(rest) > 0 {
		return doFind(key, rest, value)
	}
	return value, nil
}

func doFind(key string, rest []string, value interface{}) (interface{}, error) {
	switch value := value.(type) {
	case map[string]interface{}:
		return findMap(value, rest)
	case []interface{}:
		return findList(value, rest)
	}
	return nil, fmt.Errorf("could not find path '%s' in %s", strings.Join(rest, "."), key)
}

// Find a value within a container, using a dot-separated path of keys. Numeric
// keys can be used as indexes into an array.
func Find(node interface{}, path string) (interface{}, error) {
	return doFind("", strings.Split(path, "."), node)
}

func iterateList(l []interface{}, f func(string, interface{}) error) error {
	for idx, el := range l {
		idxStr := strconv.Itoa(idx)
		if err := f(idxStr, el); err != nil {
			return err
		}
	}
	return nil
}

func iterateMap(m map[string]interface{}, f func(string, interface{}) error) error {
	for key, el := range m {
		if err := f(key, el); err != nil {
			return err
		}
	}
	return nil
}

type exprParamWrapper struct {
	obj interface{}
}

func (w *exprParamWrapper) Get(name string) (interface{}, error) {
	switch value := w.obj.(type) {
	case map[string]interface{}:
		return Find(value, name)
	case []interface{}:
		return Find(value, name)
	}
	return nil, fmt.Errorf("not a valid context %#v", w.obj)
}

func iterate(value interface{}, f func(string, interface{}) error) error {
	switch value := value.(type) {
	case map[string]interface{}:
		return iterateMap(value, f)
	case []interface{}:
		return iterateList(value, f)
	}
	return errors.New("object is not iterable")
}

// Query a container for elements for which the (boolean) expression
// returns true. The expression is evaluated in the context of each
// object in the container. For help on the expression format see the
// govaluate documentation at https://github.com/Knetic/govaluate.
func Query(node interface{}, query string) (interface{}, error) {
	expr, err := govaluate.NewEvaluableExpression(query)
	if err != nil {
		return nil, fmt.Errorf("query syntax error: %v", err)
	}

	var out []interface{}
	err = iterate(node, func(key string, value interface{}) error {
		result, err := expr.Eval(&exprParamWrapper{value})
		if err != nil {
			return fmt.Errorf("error evaluating %s on element %s: %v", query, key, err)
		}
		if boolResult, ok := result.(bool); ok && boolResult {
			out = append(out, value)
		}
		return nil
	})
	return out, err
}

func attrNameFromFile(f os.FileInfo) (string, bool) {
	if strings.HasPrefix(f.Name(), ".") {
		return "", true
	}
	attrName := f.Name()
	if !f.IsDir() && strings.HasSuffix(attrName, ".json") {
		attrName = strings.TrimSuffix(attrName, ".json")
	}
	return attrName, false
}

func parseNodeFromFile(path string) (map[string]interface{}, error) {
	var node map[string]interface{}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &node)
	return node, err
}

func parseNodeFromPath(path string) (map[string]interface{}, error) {
	var root map[string]interface{}

	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if stat.IsDir() {
		dir, err := os.Open(path)
		if err != nil {
			return nil, err
		}
		dirents, err := dir.Readdir(-1)
		if err != nil {
			return nil, err
		}
		root = make(map[string]interface{})
		for _, fstat := range dirents {
			var leaf map[string]interface{}
			attrName, skip := attrNameFromFile(fstat)
			if skip {
				continue
			}
			fpath := filepath.Join(path, fstat.Name())
			if fstat.IsDir() {
				leaf, err = parseNodeFromPath(fpath)
			} else {
				leaf, err = parseNodeFromFile(fpath)
			}
			if err != nil {
				return nil, fmt.Errorf("%s: %v", fpath, err)
			}
			root[attrName] = leaf
		}
		err = nil
	} else {
		root, err = parseNodeFromFile(path)
	}
	if err != nil {
		return nil, err
	}
	return root, nil
}

// Parse the database found at path
func Parse(path string) (map[string]interface{}, error) {
	return parseNodeFromPath(path)
}
