package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func createTree(t testing.TB, tree map[string]string) string {
	rootDir, err := ioutil.TempDir("", "jdb-")
	if err != nil {
		t.Fatal("TempDir", err)
	}
	for key, value := range tree {
		path := filepath.Join(rootDir, key)
		dir := filepath.Dir(path)
		os.MkdirAll(dir, 0755)
		if err := ioutil.WriteFile(path, []byte(value), 0644); err != nil {
			t.Fatalf("%s: %v", path, err)
		}
	}
	return rootDir
}

func TestJDB_ParsePath(t *testing.T) {
	dir := createTree(t, map[string]string{
		"hosts/host1.json": `{"name": "host1"}`,
		"hosts/host2.json": `{"name": "host2"}`,
		"users.json":       `{"u1": {"name": "u1", "id": 1}}`,
	})
	defer os.RemoveAll(dir)

	d, err := Parse(dir)
	if err != nil {
		t.Fatal("Parse", err)
	}

	// Manually navigate the tree.
	hosts, ok := d["hosts"]
	if !ok {
		t.Fatalf("no hosts - value=%+v", d)
	}
	hostMap, ok := hosts.(map[string]interface{})
	if !ok {
		t.Fatalf("host type is wrong - value=%#v", hosts)
	}
	h1, ok := hostMap["host1"].(map[string]interface{})
	if !ok {
		t.Fatalf("no host1 - value=%+v", d)
	}
	h1Name, ok := h1["name"].(string)
	if !ok {
		t.Fatalf("no name - value=%+v", d)
	}
	if h1Name != "host1" {
		t.Fatalf("bad value for host1 name: %v", h1Name)
	}
}

func TestJDB_Find(t *testing.T) {
	dir := createTree(t, map[string]string{
		"hosts/host1.json": `{"name": "host1"}`,
		"hosts/host2.json": `{"name": "host2"}`,
		"users.json":       `{"u1": {"name": "u1", "id": 1}}`,
	})
	defer os.RemoveAll(dir)

	d, err := Parse(dir)
	if err != nil {
		t.Fatal("Parse", err)
	}

	// Find a string.
	value, err := Find(d, "hosts.host1.name")
	if err != nil {
		t.Fatal("hosts.host1.name", err)
	}
	switch value := value.(type) {
	case string:
	default:
		t.Fatalf("wrong type for hosts.host1.name: %#v", value)
	}

	// Find a hash.
	hvalue, err := Find(d, "hosts.host1")
	if err != nil {
		t.Fatal("hosts.host1", err)
	}
	switch hvalue := hvalue.(type) {
	case map[string]interface{}:
	default:
		t.Fatalf("wrong type for hosts.host1: %#v", hvalue)
	}
}

func TestJDB_FindListIndex(t *testing.T) {
	dir := createTree(t, map[string]string{
		"list.json": `{"entries": ["value1", "value2"]}`,
	})
	defer os.RemoveAll(dir)

	d, err := Parse(dir)
	if err != nil {
		t.Fatal("Parse", err)
	}

	// Find a string.
	value, err := Find(d, "list.entries.1")
	if err != nil {
		t.Fatal("list.entries.1", err)
	}
	switch value := value.(type) {
	case string:
		if value != "value2" {
			t.Fatalf("wrong value for list.entries.1: %s", value)
		}
	default:
		t.Fatalf("wrong type for list.entries.1: %#v", value)
	}
}
