package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strings"

	"git.autistici.org/ale/jdb/sqlparser"
	_ "github.com/mattn/go-sqlite3"
)

func deDot(name string) string {
	return strings.Replace(name, ".", "_", -1)
}

func getTableName(tableName *sqlparser.TableName) string {
	var name string
	if tableName.Qualifier != nil {
		name = fmt.Sprintf("%s.%s", string(tableName.Qualifier), string(tableName.Name))
	} else {
		name = string(tableName.Name)
	}
	return name
}

func findSqlTablesInSimpleTableExpr(expr sqlparser.SimpleTableExpr, tables map[string]struct{}) {
	switch expr := expr.(type) {
	case *sqlparser.Subquery:
		switch selectSubQuery := expr.Select.(type) {
		case *sqlparser.Select:
			findSqlTablesInSelect(selectSubQuery, tables)
		}
	case *sqlparser.TableName:
		// SQL does not like our usage of '.' as the path separator for
		// table specifications, so we transparently rewrite it to '_'
		// in the parsed statement itself.
		origName := getTableName(expr)
		expr.Name = []byte(deDot(origName))
		expr.Qualifier = nil
		tables[origName] = struct{}{}
	}
}

func findSqlTablesInTableExpr(expr sqlparser.TableExpr, tables map[string]struct{}) {
	switch expr := expr.(type) {
	case *sqlparser.AliasedTableExpr:
		findSqlTablesInSimpleTableExpr(expr.Expr, tables)
	case *sqlparser.ParenTableExpr:
		findSqlTablesInTableExpr(expr.Expr, tables)
	case *sqlparser.JoinTableExpr:
		findSqlTablesInTableExpr(expr.LeftExpr, tables)
		findSqlTablesInTableExpr(expr.RightExpr, tables)
	}
}

func findSqlTablesInSelect(statement *sqlparser.Select, tables map[string]struct{}) {
	for _, expr := range statement.From.Expr {
		findSqlTablesInTableExpr(expr, tables)
	}
}

func findSqlTables(statement *sqlparser.Select) []string {
	tables := make(map[string]struct{})
	findSqlTablesInSelect(statement, tables)
	var out []string
	for t := range tables {
		out = append(out, t)
	}
	return out
}

type columnSpec struct {
	name       string
	mappedName string
	ctype      int
	nullable   bool
	key        bool
}

type columnTypeGuesser struct {
	types    map[string]int
	counters map[string]int
}

const (
	columnTypeUnknown = iota
	columnTypeString
	columnTypeInt
	columnTypeBool
)

func columnTypeToString(ctype int) string {
	switch ctype {
	case columnTypeBool:
		return "boolean"
	case columnTypeInt:
		return "integer"
	default:
		return "text"
	}
}

func newColumnTypeGuesser() *columnTypeGuesser {
	return &columnTypeGuesser{
		types:    make(map[string]int),
		counters: make(map[string]int),
	}
}

// Add a new value for the specified column, use it to try to guess the column
// data type. The detection algorithm will promote any unknown (non-int,
// non-bool) value to string. It will also do the same for any mixed-value
// columns.
func (c *columnTypeGuesser) add(col string, value interface{}) {
	c.counters[col] = c.counters[col] + 1

	typ := c.types[col]
	if typ == columnTypeString {
		return
	}
	switch value.(type) {
	case int, int64:
		if typ == columnTypeUnknown {
			c.types[col] = columnTypeInt
		} else if typ != columnTypeInt {
			c.types[col] = columnTypeString
		}
	case bool:
		if typ == columnTypeUnknown {
			c.types[col] = columnTypeBool
		} else if typ != columnTypeBool {
			c.types[col] = columnTypeString
		}
	default:
		c.types[col] = columnTypeString
	}
}

func (c *columnTypeGuesser) columns(numRows int) []*columnSpec {
	var out []*columnSpec
	for col, ccount := range c.counters {
		ctype := c.types[col]
		out = append(out, &columnSpec{
			name:       col,
			mappedName: deDot(col),
			nullable:   (ccount < numRows),
			ctype:      ctype,
		})
	}
	// Identify keys (known name and nullable == false).
	for _, spec := range out {
		if (spec.name == "id" || spec.name == "name") && !spec.nullable {
			spec.key = true
		}
	}
	return out
}

type sqlTable struct {
	name    string
	columns []*columnSpec
	rows    []map[string]interface{}
}

func doFlatten(prefix string, value interface{}, values map[string]interface{}) {
	iterate(value, func(key string, subv interface{}) error {
		switch subv := subv.(type) {
		case map[string]interface{}, []interface{}:
			doFlatten(prefix+key+".", subv, values)
		default:
			values[prefix+key] = subv
		}
		return nil
	})
}

// Flatten nested iterables onto a single map. Nested attributes will be
// separeted by dots.
func flatten(node interface{}) map[string]interface{} {
	switch node.(type) {
	case map[string]interface{}, []interface{}:
		out := make(map[string]interface{})
		doFlatten("", node, out)
		return out
	default:
		return map[string]interface{}{"value": node}
	}
}

// Render values from the given node+path combination to tabular form.  Lists of
// scalar values will result in a single column named 'value'.
func toSqlTable(node interface{}, path string) (*sqlTable, error) {
	container, err := Find(node, path)
	if err != nil {
		return nil, err
	}

	ctypes := newColumnTypeGuesser()
	var rows []map[string]interface{}
	err = iterate(container, func(key string, value interface{}) error {
		row := flatten(value)
		for colName, colValue := range row {
			ctypes.add(colName, colValue)
		}
		rows = append(rows, row)
		return nil
	})
	if err != nil {
		return nil, err
	}

	table := &sqlTable{
		name:    deDot(path),
		columns: ctypes.columns(len(rows)),
		rows:    rows,
	}
	return table, nil
}

func buildCreateStatement(table *sqlTable) string {
	stmt := []string{"CREATE TABLE", table.name, "("}
	for idx, col := range table.columns {
		if idx > 0 {
			stmt = append(stmt, ",")
		}
		colStr := fmt.Sprintf("%q %s", col.mappedName, columnTypeToString(col.ctype))
		if !col.nullable {
			colStr += " NOT NULL"
		}
		stmt = append(stmt, colStr)
	}
	stmt = append(stmt, ")")
	return strings.Join(stmt, " ")
}

func buildInsertStatement(table *sqlTable) string {
	var columnNames []string
	var placeholders []string
	for _, col := range table.columns {
		columnNames = append(columnNames, col.mappedName)
		placeholders = append(placeholders, "?")
	}
	return fmt.Sprintf("INSERT INTO %s(%s) VALUES (%s)", table.name, strings.Join(columnNames, ", "), strings.Join(placeholders, ", "))
}

// Load data in tabular form onto a SQL table.
func loadSqlTable(db *sql.DB, table *sqlTable) error {
	// Create the table.
	createStr := buildCreateStatement(table)
	if *sqlDebug {
		log.Printf("executing query: %s", createStr)
	}
	if _, err := db.Exec(createStr); err != nil {
		return err
	}

	// Create indexes.
	for idx, col := range table.columns {
		if col.key {
			qstr := fmt.Sprintf("CREATE INDEX %s_idx_%d ON %s(%s)", table.name, idx, table.name, col.name)
			if *sqlDebug {
				log.Printf("executing query: %s", qstr)
			}
			if _, err := db.Exec(qstr); err != nil {
				return err
			}
		}
	}

	// Insert rows.
	insStr := buildInsertStatement(table)
	if *sqlDebug {
		log.Printf("insert query: %s", insStr)
	}
	insStmt, err := db.Prepare(insStr)
	if err != nil {
		return err
	}
	for _, row := range table.rows {
		var args []interface{}
		for _, col := range table.columns {
			if value, ok := row[col.name]; ok {
				args = append(args, value)
			} else {
				args = append(args, nil)
			}
		}
		if *sqlDebug {
			log.Printf("insert row: %+v", args)
		}
		if _, err := insStmt.Exec(args...); err != nil {
			return err
		}
	}

	return nil
}

func withInMemoryDb(f func(*sql.DB) error) error {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		return err
	}
	defer db.Close()
	return f(db)
}

func statementToString(statement sqlparser.Statement) string {
	buf := sqlparser.NewTrackedBuffer(nil)
	statement.Format(buf)
	return buf.String()
}

func rowToObj(rows *sql.Rows) (map[string]interface{}, error) {
	columns, _ := rows.Columns()
	var targets []interface{}
	for range columns {
		var s string
		targets = append(targets, &s)
	}

	err := rows.Scan(targets...)
	if err != nil {
		return nil, err
	}
	el := make(map[string]interface{})
	for idx, col := range columns {
		el[col] = *(targets[idx].(*string))
	}
	return el, nil
}

// SQLQuery runs a SQL query on the data.
func SQLQuery(node interface{}, query string) ([]interface{}, error) {
	// Analyze the query, find the referenced tables, flatten the required
	// data from JSON into an in-memory SQL database, and run the SQL query.

	statement, err := sqlparser.Parse(query)
	if err != nil {
		return nil, err
	}
	var tableRefs []string
	switch statement := statement.(type) {
	case *sqlparser.Select:
		tableRefs = findSqlTables(statement)
	default:
		return nil, errors.New("can only execute SELECT statements")
	}

	var out []interface{}
	err = withInMemoryDb(func(db *sql.DB) error {
		// Load SQL tables.
		if *sqlDebug {
			log.Printf("detected tables %+v", tableRefs)
		}
		for _, tableref := range tableRefs {
			table, err := toSqlTable(node, tableref)
			if err != nil {
				return fmt.Errorf("table %s: %v", tableref, err)
			}
			if err := loadSqlTable(db, table); err != nil {
				return fmt.Errorf("table %s: %v", tableref, err)
			}
		}

		// Run the query and convert the result to a list of maps.
		queryStr := statementToString(statement)
		if *sqlDebug {
			log.Printf("query: %s", queryStr)
		}
		rows, err := db.Query(queryStr)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			el, err := rowToObj(rows)
			if err != nil {
				return err
			}
			out = append(out, el)
		}
		return rows.Err()
	})
	return out, err
}
