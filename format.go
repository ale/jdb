package main

import (
	"encoding/json"
	"fmt"
	"io"
)

type formatter interface {
	Print(io.Writer, interface{})
}

type plainFormatter struct{}

func (p plainFormatter) Print(w io.Writer, value interface{}) {
	switch value := value.(type) {
	case map[string]interface{}:
		// Print the keys.
		for k, v := range value {
			fmt.Fprintf(w, "%s: ", k)
			p.Print(w, v)
		}

	case []interface{}:
		// Print the values.
		for _, v := range value {
			p.Print(w, v)
			fmt.Fprintf(w, "\n")
		}

	default:
		fmt.Fprintf(w, "%v\n", value)
	}
}

type jsonFormatter struct{}

func (j jsonFormatter) Print(w io.Writer, value interface{}) {
	if data, err := json.MarshalIndent(value, "", "    "); err == nil {
		w.Write(data)
		io.WriteString(w, "\n")
	}
}
