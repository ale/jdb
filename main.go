package main

import (
	"flag"
	"io"
	"log"
	"os"
	"strings"

	"github.com/google/subcommands"

	"golang.org/x/net/context"
)

var (
	dbDir    = flag.String("db", "/etc/jdb", "database path")
	sqlDebug = flag.Bool("debug", false, "enable debugging output")

	// Private reference to stdout, overridden for testing.
	stdout io.Writer = os.Stdout
)

func getFormatter(fmt string) formatter {
	switch fmt {
	case "json":
		return &jsonFormatter{}
	case "text", "plain":
		return &plainFormatter{}
	default:
		log.Fatalf("unknown formatter '%s'", fmt)
	}
	return nil
}

type getCmd struct {
	format string
}

func (l *getCmd) Name() string     { return "get" }
func (l *getCmd) Synopsis() string { return "Print contents/elements of a node." }
func (l *getCmd) Usage() string {
	return `get [--format=FMT] <path>
  Print content/elements of a node.
`
}

func (l *getCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&l.format, "format", "json", "output format")
}

func (l *getCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	root, err := Parse(*dbDir)
	if err != nil {
		log.Printf("Could not initialize database: %v", err)
		return subcommands.ExitFailure
	}

	path := f.Arg(0)
	node, err := Find(root, path)
	if err != nil {
		log.Printf("Find: %v", err)
		return subcommands.ExitFailure
	}
	fmt := getFormatter(l.format)
	fmt.Print(stdout, node)
	return subcommands.ExitSuccess
}

type queryCmd struct {
	format string
}

func (l *queryCmd) Name() string     { return "query" }
func (l *queryCmd) Synopsis() string { return "Find nodes matching an expression." }
func (l *queryCmd) Usage() string {
	return `query [--format=FMT] <path> <expr>
  Query a set of nodes for those matching an expression.
`
}

func (l *queryCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&l.format, "format", "json", "output format")
}

func (l *queryCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 2 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	root, err := Parse(*dbDir)
	if err != nil {
		log.Printf("Could not initialize database: %v", err)
		return subcommands.ExitFailure
	}

	path := f.Arg(0)
	query := f.Arg(1)
	node, err := Find(root, path)
	if err != nil {
		log.Printf("Find: %v", err)
		return subcommands.ExitFailure
	}
	result, err := Query(node, query)
	if err != nil {
		log.Printf("Query: %v", err)
		return subcommands.ExitFailure
	}

	fmt := getFormatter(l.format)
	fmt.Print(stdout, result)
	return subcommands.ExitSuccess
}

type sqlCmd struct {
	format string
}

func (l *sqlCmd) Name() string     { return "sql" }
func (l *sqlCmd) Synopsis() string { return "Execute a SQL query." }
func (l *sqlCmd) Usage() string {
	return `sql [--format=FMT] <query>
  Execute a SQL query.
`
}

func (l *sqlCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&l.format, "format", "json", "output format")
}

func (l *sqlCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	query := strings.Join(f.Args(), " ")
	if query == "" {
		log.Printf("Not enough arguments")
		return subcommands.ExitFailure
	}

	root, err := Parse(*dbDir)
	if err != nil {
		log.Printf("Could not initialize database: %v", err)
		return subcommands.ExitFailure
	}
	results, err := SQLQuery(root, query)
	if err != nil {
		log.Printf("Error: %v", err)
		return subcommands.ExitFailure
	}

	fmt := getFormatter(l.format)
	fmt.Print(stdout, results)
	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")
	subcommands.Register(&getCmd{}, "")
	subcommands.Register(&queryCmd{}, "")
	subcommands.Register(&sqlCmd{}, "")
}

func runMain() int {
	log.SetFlags(0)
	flag.Parse()
	return int(subcommands.Execute(context.Background()))
}

func main() {
	log.SetFlags(0)
	os.Exit(runMain())
}
