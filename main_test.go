package main

import (
	"bytes"
	"os"
	"testing"
)

func runTestCmd(t testing.TB, args []string, expectedStatus int, expectedOutput string) {
	os.Args = args
	var buf bytes.Buffer
	stdout = &buf
	status := runMain()
	if status != expectedStatus {
		t.Fatalf("error running %v: status=%d expected=%d", args, status, expectedStatus)
	}
	output := buf.String()
	if output != expectedOutput {
		t.Fatalf("bad output for command %v: %s, expected %s", args, output, expectedOutput)
	}
}

func TestMain_Get(t *testing.T) {
	*dbDir = createTree(t, map[string]string{
		"hosts/host1.json": `{"name": "host1"}`,
		"hosts/host2.json": `{"name": "host2"}`,
		"users.json":       `{"u1": {"name": "u1", "id": 1}}`,
		"list.json":        `{"entries": ["a", "b"]}`,
	})
	defer os.RemoveAll(*dbDir)

	runTestCmd(t, []string{"jdb", "get", "--format=json", "hosts.host1.name"}, 0, `"host1"`)
	runTestCmd(t, []string{"jdb", "get", "--format=json", "users.u1"}, 0, `{
    "id": 1,
    "name": "u1"
}`)
	runTestCmd(t, []string{"jdb", "get", "--format=json", "list.entries"}, 0, `[
    "a",
    "b"
]`)

	runTestCmd(t, []string{"jdb", "get", "--format=text", "list.entries"}, 0, `a
b
`)
	runTestCmd(t, []string{"jdb", "get", "--format=text", "hosts"}, 0, `host2
host1
`)

	runTestCmd(t, []string{"jdb", "get", "--format=json", "banana"}, 1, "")
	runTestCmd(t, []string{"jdb", "get", "--format=json", "banana.banana"}, 1, "")
}

func TestMain_Query(t *testing.T) {
	*dbDir = createTree(t, map[string]string{
		"hosts/host1.json": `{"name": "host1", "addr": {"ipv4": "1.2.3.4"}}`,
		"hosts/host2.json": `{"name": "host2", "addr": {"ipv4": "2.3.4.5"}}`,
		"hostlist.json":    `{"entries": [{"name": "host1"}, {"name": "host2"}]}`,
	})
	defer os.RemoveAll(*dbDir)

	runTestCmd(t, []string{"jdb", "query", "hosts", "name == 'host2'"}, 0, `[
    {
        "addr": {
            "ipv4": "2.3.4.5"
        },
        "name": "host2"
    }
]`)
	runTestCmd(t, []string{"jdb", "query", "hosts", "[addr.ipv4] == '2.3.4.5'"}, 0, `[
    {
        "addr": {
            "ipv4": "2.3.4.5"
        },
        "name": "host2"
    }
]`)
	runTestCmd(t, []string{"jdb", "query", "hostlist.entries", "name == 'host2'"}, 0, `[
    {
        "name": "host2"
    }
]`)
}

func TestMain_Sql(t *testing.T) {
	*dbDir = createTree(t, map[string]string{
		"config/hosts/host1.json":  `{"name": "host1", "addr": {"ipv4": "1.2.3.4"}}`,
		"config/hosts/host2.json":  `{"name": "host2", "addr": {"ipv4": "2.3.4.5"}}`,
		"config/hosts/host3.json":  `{"name": "host3", "addr": {"ipv4": "3.4.5.6", "ipv6": "2001:1:2::3"}}`,
		"config/network/net1.json": `{"name": "net1", "hosts": ["host1", "host2"]}`,
	})
	defer os.RemoveAll(*dbDir)

	runTestCmd(t, []string{"jdb", "sql", "select host.addr_ipv4 from config.hosts as host join config.network.net1.hosts as net on net.value = host.name order by host.addr_ipv4 asc limit 1"}, 0, `[
    {
        "addr_ipv4": "1.2.3.4"
    }
]`)
}
